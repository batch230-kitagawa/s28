// CRUD Operations
/*
	- CRUD operations are the heart of any backend application
	- Mastering the CRUD operations is essential for any developer
	- This helps in building character and increasing exposure to
	logical statements that will help us manipulate our data
	- Mastering the CRUD operations of any language makes us a valuable
	developer and makes the work easier for us to deal with huge amounts
	of information
*/

// [SECTION] Inserting Documents
/*
	Syntax:
		- db.collectionName.insertOne({object});
*/

// Insert One
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact:{
		phone: "87654321",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

// Insert Many
/*
	- Syntax
	- db.collectionName.insertMany([{objectA}, {objectB}]);
*/

db.users.insertMany([
	{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact:{
		phone: "87000",
		email: "stephenhawking@gmail.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "none"
	},
	{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact:{
		phone: "4770220",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["Laravel", "React", "MongoDB"],
	department: "none"
	}
]
)

// [SECTION] Finding Documents (READ)
/*
	- db.collectionName.find();
	- db.collectionName.find({field: value});

*/

// Retrieve all documents
db.users.find();

// Retrieves specific document/s
db.users.find({firstName: "Stephen"});

// multiple criteria
db.users.find({lastname: "Armstrong", age:82});

// [SECTION] Update and Replace (Update)

// Inserting something we want to update
db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});
/*
	Syntax:
		- db.collectionName.updateOne({criteria}, {$set: {field/s: value/s}})
		*/

db.users.updateOne(
	{
		firstName: "Test"
	},
	{
		$set: {
			firstName: "Bill",
			lastname: "Gates",
			age: 65,
			contact: {
				phone: "1234567",
				email: "imrich@gmail.com"
					},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
	)

db.users.find();

/////////////////

db.users.updateOne(
	{firstName: "Jane"},
	{
		$set: 
		{
			firstName:"Jenny"
		}
	}
)

db.users.find();

// Updating multiple documents that matches field and value
db.users.updateMany(
	{department: "none"},
	{
		$set:
		{
			department: "HR"
		}
	})

db.users.find();

// Replaces all the fields in a document/object

db.users.replaceOne(
{firstName: "Bill"},
{
	firstName: "Elon",
	lastname: "Musk",
	age: 30,
	courses: ["PHP", "Laravel", "HTML"],
	status: "tripings"
}
)

db.users.find();

// [SECTION] Delete/DELETE
/*
	db.collectionName.deleteOne({field: value})
	db.collectionName.deleteOne({field: value})
*/

// delete one document that matches the field and value
db.users.deleteOne({
	firstName: "Jane";
})

// deletes all documents that satisfies the field and value
db.users.deleteMany({
	firstName: "Neil";
})

db.users.deleteMany({
	department: "HR"
})

db.users.find();



